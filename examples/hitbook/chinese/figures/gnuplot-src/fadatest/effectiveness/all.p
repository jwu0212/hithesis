#file = "spec.dat"
#set terminal postscript eps enhanced color solid "Times-Roman" 45 size 3,3.5
set term pdfcairo  font "Times New Roman, 22.5" size 15,7.5
set output 'effe-all.pdf'
#set datafile separator ','
set rmargin 1.6
set lmargin 6.6
set bmargin 2.2
set tmargin 1.6

set xtics scale 0.5

#set size 6.03,3
set origin 0.1,-0.06
set multiplot
#set yrange [0:800]

unset key

#outname = sprintf("%s.png", substr(file, 0 ,strlen(file)-4))
#set output outname

set size 0.205,0.29
set origin 0,0.25
set title 'blackscholes' offset 0,-2.15
#set format y '%.0f'
#set key left
set xrange [-0.2:7.2]
set yrange [0:7]
#unset xtics
set xtics("2.9.1" 0, "2.12.1" 1, "3.0.0" 2, "4.0.1" 3, "4.1.1" 4, "4.2.0" 5, "5.0.0" 6, "5.2.0" 7,)
set xtics right rotate by 45 offset 0.5,0.3
set ytics "2,,"

#set grid

plot 'blackscholes.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'blackscholes.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'
set ytics "1,,"


set size 0.205,0.29
set origin 0.2,0.25
set title 'bodytrack'
set yrange [0:6]
#set key left
unset ylabel
#unset ytics

#set grid

plot 'bodytrack.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'bodytrack.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.4,0.25
set title 'canneal'
set yrange [0:3]
#set key left
unset ylabel

#set grid

plot 'canneal.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'canneal.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.6,0.25
set title 'dedup'
#set key left
unset ylabel

#set grid

plot 'dedup.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'dedup.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.8,0.25
set title 'fluidanimate'
set yrange [0:8]
#set key left
unset ylabel
set ytic 2

#set grid

plot 'fluidanimate.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'fluidanimate.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set ytic 1


#set yrange [0:800]
set origin 0,0
set title 'freqmine'
set yrange [0.5:2]
##set key left
set size 0.205,0.29
#set ylabel ' '
set ytics
unset key

#set grid

plot 'freqmine.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'freqmine.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.2,0
set size 0.205,0.29
set title 'streamcluster'
set yrange [0:9]
set ytics "1,,"
#set key left
unset ylabel
#unset ytics
set ytic 3

#set grid

plot 'streamcluster.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'streamcluster.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.4,0
set title 'swaptions'
set yrange [0:6]
set ytics "1,,"
#set key left
unset ylabel

#set grid

plot 'swaptions.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'swaptions.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.6,0
set title 'vips'
unset ylabel
set key left at 9.2,4.5 

#set grid

plot 'vips.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'vips.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

unset key

set size 0.205,0.29
set origin 0,0.75
set title 'perlbench' offset 0,-2.15
#set format y '%.0f'
set xrange [-0.2:7.2]
set yrange [0:2.5]
#unset xtics
set xtics("2.9.1" 0, "2.12.1" 1, "3.0.0" 2, "4.0.1" 3, "4.1.1" 4, "4.2.0" 5, "5.0.0" 6, "5.2.0" 7,)
#set xtics right rotate by 30 offset 1.6,0
set ytics "1,,"

#set grid

plot 'perlbench.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'perlbench.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'



set size 0.205,0.29
set origin 0.2,0.75
set title 'gcc'
#set yrange [0:6]
#set key left
unset ylabel
#unset ytics

#set grid

plot 'gcc.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'gcc.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.4,0.75
set title 'mcf'
#set yrange [0:3]
#set key left
unset ylabel

#set grid

plot 'mcf.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'mcf.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.6,0.75
set title 'omnetpp'
#set key left
unset ylabel

#set grid

plot 'omnetpp.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'omnetpp.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.8,0.75
set title 'xalancbmk'
#set yrange [0:8]
#set key left
unset ylabel

#set grid

plot 'xalancbmk.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'xalancbmk.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'


##set yrange [0:800]
set origin 0,0.5
set title 'x264'
#set yrange [0.5:2]
##set key left
set size 0.205,0.29
#set ylabel ' '
set ytics
#set ylabel 'Normalized Execution Time' offset 0.5,-5.3
#unset key

#set grid

plot 'x264.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'x264.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.2,0.5
set size 0.205,0.29
set title 'deepsjeng'
#set yrange [0:9]
set ytics "1,,"
#set key left
unset ylabel
#unset ytics

#set grid

plot 'deepsjeng.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'deepsjeng.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.4,0.5
set title 'leela'
#set yrange [0:6]
set ytics "1,,"
#set key left
unset ylabel

#set grid

plot 'leela.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'leela.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.6,0.5
set title 'exchange2'
#set key left at 8.5,4.5 font ",45"
unset ylabel

#set grid

plot 'exchange2.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'exchange2.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'

set origin 0.8,0.5
set title 'xz'
#set key left at 8.5,4.5 font ",45"
unset ylabel

#set grid

plot 'xz.dat' u 1:($2) with linespoints lc black lw 1 ps 1.25 pt 6 title '原始SPEC',\
    'xz.dat' u 1:($3) with linespoints lc black lw 1 ps 1.25 pt 8 title 'FADATest'


set ylabel "标准化执行时间" offset 0,22
unset title
set origin 0,-0.3
plot [][0:1] -1     # plot a dummy line out of range
