#file = "spec.dat"
#set terminal postscript eps enhanced color solid "Times-Roman" 45 size 3,3
set term pdfcairo  font "Times New Roman, 15.5" size 6,2
set output 'effe-vg.pdf'
#set datafile separator ','
set lmargin 5.5
set rmargin 1
set bmargin 2.2
set tmargin 1.6

set size 3.16,0.92
set origin -0.15,-0.14
set multiplot
#set yrange [0:800]

unset key

#outname = sprintf("%s.png", substr(file, 0 ,strlen(file)-4))
#set output outname

#set yrange [0:800]
set origin 0,0
set title 'perlbench' offset 0,-2.35
#set yrange [0.5:2]
#set key left
set size 0.333,0.95
set ylabel "标准化执行时间" offset 2.38,-0.5
set ytics 0.5 offset 0.5,0
set xtics("3.14.0" 0, "3.15.0" 1, "3.16.0" 2, "3.17.0" 3, "3.18.0" 4)
set xrange [0:4]
set xtics right rotate by 45 offset 0.5,0.3

#set grid

plot 'perlbench.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title 'Original',\
    'perlbench.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest'

set origin 0.333,0
set title 'omnetpp'
set yrange [0.35:1.5]
set ytics "0.5,,"
set key at 4.5,1.775   maxrows 1
unset ylabel
#unset ytics
#set ytic 3

#set grid

plot 'omnetpp.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title '原始程序',\
    'omnetpp.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest'

unset key
set origin 0.666,0
set title 'dedup'
set yrange [0.8:1.2]
set ytics "0.1,,"
#set key left
unset ylabel

#set grid

plot 'dedup.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title 'Original',\
    'dedup.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest'

