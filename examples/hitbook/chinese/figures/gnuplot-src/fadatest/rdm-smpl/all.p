#file = "spec.dat"
set terminal postscript eps enhanced color solid "Times-Roman" 45 size 3,3.5
set output 'effe-all.eps'
#set datafile separator ','
set rmargin 1.6
set bmargin 2.2
set tmargin 1.6

set size 6.03,3
set origin 0.1,-0.06
set multiplot
#set yrange [0:800]

unset key

#outname = sprintf("%s.png", substr(file, 0 ,strlen(file)-4))
#set output outname

set size 1.25,0.8
set origin 0.2,0.7
set title 'blackscholes' offset 0,-2.15
#set format y '%.0f'
#set key left
set xrange [-0.2:7.2]
set yrange [0:7]
#unset xtics
set xtics("2.9.1" 0, "2.12.1" 1, "3.0.0" 2, "4.0.1" 3, "4.1.1" 4, "4.2.0" 5, "5.0.0" 6, "5.2.0" 7,)
set xtics right rotate by 45 offset 0.5,0.3
set ytics "2,,"

#set grid

plot 'blackscholes.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'blackscholes.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'
set ytics "1,,"


set size 1.25,0.8
set origin 1.35,0.7
set title 'bodytrack'
set yrange [0:6]
#set key left
unset ylabel
#unset ytics

#set grid

plot 'bodytrack.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'bodytrack.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 2.5,0.7
set title 'canneal'
set yrange [0:3]
#set key left
unset ylabel

#set grid

plot 'canneal.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'canneal.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 3.65,0.7
set title 'dedup'
#set key left
unset ylabel

#set grid

plot 'dedup.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'dedup.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 4.81,0.7
set title 'fluidanimate'
set yrange [0:8]
#set key left
unset ylabel
set ytic 2

#set grid

plot 'fluidanimate.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'fluidanimate.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set ytic 1


#set yrange [0:800]
set origin 0.2,0
set title 'freqmine'
set yrange [0.5:2]
##set key left
set size 1.25,0.8
#set ylabel ' '
set ytics
unset key

#set grid

plot 'freqmine.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'freqmine.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 1.35,0
set size 1.25,0.8
set title 'streamcluster'
set yrange [0:9]
set ytics "1,,"
#set key left
unset ylabel
#unset ytics
set ytic 3

#set grid

plot 'streamcluster.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'streamcluster.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 2.5,0
set title 'swaptions'
set yrange [0:6]
set ytics "1,,"
#set key left
unset ylabel

#set grid

plot 'swaptions.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'swaptions.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 3.65,0
set title 'vips'
unset ylabel
set key left at 9.2,4.5 

#set grid

plot 'vips.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'vips.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

unset key

set size 1.25,0.8
set origin 0.2,2.1
set title 'perlbench' offset 0,-2.15
#set format y '%.0f'
set xrange [-0.2:7.2]
set yrange [0:2.5]
#unset xtics
set xtics("2.9.1" 0, "2.12.1" 1, "3.0.0" 2, "4.0.1" 3, "4.1.1" 4, "4.2.0" 5, "5.0.0" 6, "5.2.0" 7,)
#set xtics right rotate by 30 offset 1.6,0
set ytics "1,,"

#set grid

plot 'perlbench.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'perlbench.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'



set size 1.25,0.8
set origin 1.35,2.1
set title 'gcc'
#set yrange [0:6]
#set key left
unset ylabel
#unset ytics

#set grid

plot 'gcc.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'gcc.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 2.5,2.1
set title 'mcf'
#set yrange [0:3]
#set key left
unset ylabel

#set grid

plot 'mcf.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'mcf.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 3.65,2.1
set title 'omnetpp'
#set key left
unset ylabel

#set grid

plot 'omnetpp.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'omnetpp.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 4.81,2.1
set title 'xalancbmk'
#set yrange [0:8]
#set key left
unset ylabel

#set grid

plot 'xalancbmk.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'xalancbmk.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'


##set yrange [0:800]
set origin 0.116,1.4
set title 'x264'
#set yrange [0.5:2]
##set key left
set size 1.332,0.8
set ylabel ' '
set ytics
set ylabel 'Normalized Execution Time' offset 0.5,-5.3
#unset key

#set grid

plot 'x264.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'x264.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 1.35,1.4
set size 1.25,0.8
set title 'deepsjeng'
#set yrange [0:9]
set ytics "1,,"
#set key left
unset ylabel
#unset ytics

#set grid

plot 'deepsjeng.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'deepsjeng.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 2.5,1.4
set title 'leela'
#set yrange [0:6]
set ytics "1,,"
#set key left
unset ylabel

#set grid

plot 'leela.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'leela.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 3.65,1.4
set title 'exchange2'
#set key left at 8.5,4.5 font ",45"
unset ylabel

#set grid

plot 'exchange2.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'exchange2.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

set origin 4.81,1.4
set title 'xz'
#set key left at 8.5,4.5 font ",45"
unset ylabel

#set grid

plot 'xz.dat' u 1:($2) with linespoints lc black lw 1 ps 3.8 pt 6 title 'Original',\
    'xz.dat' u 1:($3) with linespoints lc black lw 1 ps 3.8 pt 8 title 'MODBT'

