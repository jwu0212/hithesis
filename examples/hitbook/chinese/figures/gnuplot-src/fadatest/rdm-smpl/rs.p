#file = "spec.dat"
set term pdfcairo  font "Times New Roman, 15.5" size 6,2
set output 'effe-rs.pdf'
#set datafile separator ','
set lmargin 5.5
set rmargin 1
set bmargin 2.2
set tmargin 1.6

set size 3.16,0.89
set origin -0.15,-0.14
set multiplot
#set yrange [0:800]

unset key

#outname = sprintf("%s.png", substr(file, 0 ,strlen(file)-4))
#set output outname

#set yrange [0:800]
set origin 0,0
set title 'canneal' offset 0,-2.35
set yrange [0.5:3]
#set key left
set size 0.333,0.95
set ylabel "标准化执行时间" offset 2.5,-0.5
set ytics 0.5 offset 0.5,0
set xtics("2.9.1" 0, "2.12.1" 1, "3.0.0" 2, "4.0.1" 3, "4.1.1" 4, "4.2.0" 5, "5.0.0" 6, "5.2.0" 7,)
#set xtics font ", 38"
set xrange [-0.2:7.2]
set xtics right rotate by 45 offset 0.5,0.3

#set grid

plot 'canneal.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title 'Original',\
    'canneal.dat' u 1:($4) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest',\
    'canneal.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 7 title 'RdmSmpl'

set origin 0.333,0
#set size 1,0.9
set title 'dedup'
set yrange [0.7:2.5]
set ytics "0.5,,"
set key at 13.01,2.85   maxrows 1
unset ylabel
#unset ytics
#set ytic 3

#set grid

plot 'dedup.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title '原始程序',\
    'dedup.dat' u 1:($4) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest',\
    'dedup.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 7 title '随机采样'

unset key
set origin 0.666,0
set title 'freqmine'
set yrange [0.8:2]
set ytics "0.5,,"
#set key left
unset ylabel

#set grid

plot 'freqmine.dat' u 1:($2) with linespoints lc black lw 1 ps 1.2 pt 6 title 'Original',\
    'freqmine.dat' u 1:($4) with linespoints lc black lw 1 ps 1.2 pt 8 title 'FADATest',\
    'freqmine.dat' u 1:($3) with linespoints lc black lw 1 ps 1.2 pt 7 title 'RdmSmpl'

