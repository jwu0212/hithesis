set term pdfcairo  font "Times New Roman, 9.5" size 3,2
set output 'performance.pdf'
n=2
offset=-0.12

set lmargin 9.5
set rmargin 0.1
#set bmargin 1.5

total_box_width_relative=0.70
gap_width_relative=0 #0.1
dx=total_box_width_relative/n
#d_width=(gap_width_relative+total_box_width_relative)*dx/2.
#reset

set key top

#set label "345" right at graph 0.358,1.05 

set xtics right offset 0.75,0.38
#set xtics("x1" 1, "x7" 7, "x15" 15, "x23" 23, "x31" 31)
set xtics 
set ytics 

set xtics scale 0
set ytics scale 0

#set xrange [-1:19]
set yrange [:21000]
#set term png truecolor size 800,400
#set output "normalized_time.png"
#set xlabel "(a) x86-64" offset 0,0.8
set ylabel "运行时间 (s)" offset 0.5,0
set grid ytics
set xtics right rotate by 30 offset 1,0
set boxwidth total_box_width_relative/n
#set style fill transparent solid 1 noborder
set style fill pattern 2
plot "perf.dat" u ($1-dx*0.5):3:xtic(2) w boxes lc black fill pattern 3 title "QEMU",\
     "perf.dat" u ($1+dx*0.5):4 w boxes lc black fill pattern 7 title "WDBT"
