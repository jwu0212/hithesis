#set terminal postscript eps enhanced color solid "Times-Roman" 14 size 3,1.6
set term pdfcairo  font "Times New Roman, 9.5" size 3,2
set output 'cc_cpu_st.pdf'
n=4
offset=-0.12

set lmargin 8
set rmargin 0.1
set tmargin 2
set bmargin 3

total_box_width_relative=0.85
gap_width_relative=0 #0.1
dx=total_box_width_relative/n
#d_width=(gap_width_relative+total_box_width_relative)*dx/2.
#reset

#set key at 9.5,75 

#set label "345" right at graph 0.358,1.05 

set xtics("perlbench" 1, "gcc" 2, "mcf" 3, "omnetpp" 4, "xalancbmk" 5, "x264" 6, "deepsjeng" 7, "leela" 8, "exchange2" 9, "xz" 10, "ave" 11.5)
set xtics right rotate by 30 offset 1.6,0
set xtics 
set ytics 

set key at 9.5,110
set key vertical maxrows 1
#set key invert

set origin 0,0
set yrange [0:]
#set xrange [0:6]
#set xlabel "benchmark"
set ylabel "内存写入百分比 (%)" offset 0.8,0
set grid ytics
set boxwidth total_box_width_relative/n*2.5
set style fill pattern 2
plot "cc_cpu_st.dat" u 1:($4*100) w boxes lc black fill pattern 0 title "其它",\
     "cc_cpu_st.dat" u 1:($3*100) w boxes lc black fill pattern 2 title "条件码",\
     "cc_cpu_st.dat" u 1:($2*100) w boxes lc black fill pattern 3 title "寄存器"
