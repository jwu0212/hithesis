#parsec 16 Threads Aarch64 on QEMU-simd-v1.2
#set term pdf
set term pdfcairo  font "Times New Roman, 9.5" size 3,2
set out "PARSEC-riscv64-AVX-16.pdf"
set sty data hist
set xtic rotate by 30 right
set ylabel '性能加速 (倍)'
set grid ytics lw 2
set tics nomirror
set xtics scale 0
set yrange [0.8:1.2]
plot "PARSEC-riscv64-AVX-16.dat" using 2:xtic(1) fill pattern 3 black ti col\
         , '' us 3 fill pattern 4 black ti col;
