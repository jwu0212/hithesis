#########################################################################
# File Name: run-all.sh
# Author: Jin Wu
# mail: jw0212@gmail.com
# Created Time: Sat 13 Jun 2020 03:17:13 AM EDT
#########################################################################
#!/bin/bash
ls *.hotness|while read line
do
	python ./rank2.py $line
done
